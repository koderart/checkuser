package utils
{
    import flash.events.Event;
    import flash.events.IEventDispatcher;
    import flash.events.IOErrorEvent;
    import flash.events.SecurityErrorEvent;
    import flash.net.FileFilter;
    import flash.net.FileReference;
    import flash.utils.Dictionary;
    import spark.components.Alert;

    public class XMLLoader
    {
        private static var _instance: XMLLoader;
        private var requestsDictionary:Dictionary = new Dictionary(true);

        public static function get instance():XMLLoader
        {
            if (_instance == null) {
                _instance = new XMLLoader();
            }
            return _instance;
        }

        public function XMLLoader()
        {
        }

        public function sendRequest(resultHandler:Function):void
        {
            var file:FileReference = new FileReference();
            configureListeners(file);

            if (resultHandler != null) {
                requestsDictionary[file] = resultHandler;
            } else {
                requestsDictionary[file] = true;
            }

            file.browse([new FileFilter("XML", "*.xml"), new FileFilter("ALL", "*")]);
        }

        private function configureListeners(dispatcher:IEventDispatcher):void
        {
            dispatcher.addEventListener(Event.CANCEL, cancelHandler);
            dispatcher.addEventListener(Event.COMPLETE, completeHandler);
            dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
            dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
            dispatcher.addEventListener(Event.SELECT, selectHandler);
        }

        private function selectHandler(event:Event):void
        {
            var file:FileReference = FileReference(event.target);
            file.load();
        }

        private function completeHandler(event:Event):void
        {
            var file:FileReference = FileReference(event.target);
            var data:Object;
            try {
                data = new XML(file.data);
            } catch (e:Error) {
                Alert.show("Ошибка парсинга XML");
                return;
            }

            if (requestsDictionary[file] && requestsDictionary[file] is Function) {
                requestsDictionary[file].call(null, data);
            }

            delete requestsDictionary[file];
        }

        private function cancelHandler(event:Event):void
        {
            delete requestsDictionary[event.target];
        }

        private function ioErrorHandler(event:IOErrorEvent):void
        {
            delete requestsDictionary[event.target];
            Alert.show("Ошибка загрузки файла");
        }

        private function securityErrorHandler(event:SecurityErrorEvent):void
        {
            delete requestsDictionary[event.target];
            Alert.show("Ошибка загрузки файла");
        }
    }
}
