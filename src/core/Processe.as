package core
{
    public class Processe
    {
        private var _type:String;

        public function Processe(data:XML=null)
        {
            if (data) {
                parseData(data);
            }
        }

        public function get type():String
        {
            return _type;
        }

        private function parseData(data:XML):void
        {
            _type = String(data.@type);
        }
    }
}
