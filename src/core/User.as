package core
{

    public class User
    {
        private var _level : int;
        private var _units : Vector.<Unit>;
        private var _processes : Vector.<Processe>;

        public function User(data:XML=null)
        {
            if (data) {
                parseData(data);
            }
        }

        public function get level():int
        {
            return _level;
        }

        public function get units():Vector.<Unit>
        {
            return _units;
        }

        public function get processes():Vector.<Processe>
        {
            return _processes;
        }

        private function parseData(data:XML):void
        {
            _level = int(data.@level);
            _units = new <Unit>[];
            var unitsList:XMLList = new XMLList(data.units.unit);
            for each (var u:XML in unitsList) {
                _units.push(new Unit(u));
            }

            _processes = new <Processe>[];
            for each (var p:XML in data.processes.children()) {
                if (!p.name()) continue;

                switch (p.name().localName.toLowerCase()) {
                    case "upgrade_unit":
                        _processes.push(new Processe(p));
                        break;
                }
            }
        }
    }
}
