package core
{
    public class Conditions
    {
        private var _name : String;
        private var _type : String;
        private var _level : int;
        private var _children : Vector.<Conditions>;
        private var _message:String = "Ошибка в условиях. Проверьте XML";

        public function Conditions(data:XML=null)
        {
            if (data) {
                parseData(data);
            }
        }

        public function check(user:User):Boolean
        {
            var result:Boolean = false;

            switch (_name) {
                case "and":
                    result = true;
                    for each (var condition:Conditions in _children) {
                        if (!condition.check(user)) {
                            result = false;
                            _message = condition.message;
                        }
                    }
                    break;
                case "or":
                    for each (condition in _children) {
                        if (condition.check(user)) {
                            result = true;
                        }
                    }
                    break;
                case "not":
                    for each (condition in _children) {
                        result = !condition.check(user);
                        _message = condition.message;
                    }
                    break;
                case "user_level_gt":
                    result = user.level > _level;
                    _message = "Уровень игрока должен быть больше " + _level;
                    break;
                case "unit_level_eq":
                    result = false;
                    for each (var unit:Unit in user.units) {
                        if (unit.type == _type && unit.level == _level) {
                            result = true;
                        }
                    }
                    _message = "Уровень юнита "+ _type +" должен быть равен " + _level;
                    break;
                case "unit_upgrade_started":
                    result = false;
                    for each (var upgrade:Processe in user.processes) {
                        if (upgrade.type == _type) {
                            result = true;
                        }
                    }
                    _message = "Процесс улучшения уже запущен";
                    break;
            }

            return result;
        }

        public function get message():String
        {
            return _message;
        }

        private function parseData(data:XML):void
        {
            if (!data.name()) return;

            _name = data.name().localName.toLowerCase();
            _type = data.@type;
            _level = int(data.@level);
            _children = new <Conditions>[];
            for each (var x:XML in data.children()) {
                _children.push(new Conditions(x));
            }
        }
    }
}