package core
{
    public class Unit
    {
        private var _type:String;
        private var _level:int;

        public function Unit(data:XML=null)
        {
            if (data) {
                parseData(data);
            }
        }

        public function get type():String
        {
            return _type;
        }

        public function get level():int
        {
            return _level;
        }

        private function parseData(data:XML):void
        {
            _type = String(data.@type);
            _level = int(data.@level);
        }
    }
}
